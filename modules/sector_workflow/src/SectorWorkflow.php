<?php

namespace Drupal\sector_workflow;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A utility class to improve the core workflows user interface and experience.
 *
 * Example:
 * @code
 * \Drupal::classResolver(SectorWorkflow::class)->getRevisionDate($revision);
 * @endcode
 */
class SectorWorkflow implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The content moderation moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * Instantiates a SectorWorkflow object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RouteMatchInterface $routeMatch, ModerationInformationInterface $moderationInfo) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->moderationInfo = $moderationInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('content_moderation.moderation_information'),
    );
  }

  /**
   * Get the new text for the local task titles depending on the route passed.
   *
   * @param $taskName
   * @return TranslatableMarkup
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNewTextForLocalTask($taskName): TranslatableMarkup {
    $out = NULL;

    switch ($taskName) {
      case 'content_moderation.workflows:node.latest_version_tab':
        $out = $this->t('View draft');
        break;
      case 'entity.node.edit_form':
        /* @var $node \Drupal\node\NodeInterface */
        $node = $this->routeMatch->getParameter('node');
        if ($node) {
          $out = $this->t('Edit draft');
          $parentNode = \Drupal::entityTypeManager()->getStorage('node')->load($node->id());
          if (!$this->moderationInfo->hasPendingRevision($node) && $parentNode->isPublished()) {
            $out = $this->t('New draft');
          }
        }
        break;
      case 'entity.node.canonical':
        $out = $this->t('View published');
        $routeName = $this->routeMatch->getRouteName();
        /* @var $node \Drupal\node\NodeInterface */
        $node = $this->routeMatch->getParameter('node');
        if (!$node->isPublished()) {
          $out = $this->t('View draft');
          if ($routeName == 'entity.node.latest_version' || $routeName == 'entity.node.edit_form') {
            // TODO inject this
            $parentNode = \Drupal::entityTypeManager()->getStorage('node')->load($node->id());
            if ($parentNode->isPublished()) {
              $out = $this->t('View published');
            }
          }
        }
        break;
    }

    return $out;
  }

  /**
   * Gets the (formatted) date from the given revision.
   *
   * @param $revision
   * @return string
   * @throws \Exception
   */
  public function getRevisionDate(NodeInterface $revision, $format = 'l \t\h\e jS \of F Y h:i:s A') {
    $revisionTimestamp = $revision->getRevisionCreationTime();
    $revisionDateObj = new \DateTime();
    $revisionDateObj->setTimestamp($revisionTimestamp);
    return $revisionDateObj->format($format);
  }

  /**
   * Gets the author for the given revision.
   *
   * @param $revision
   * @return mixed
   */
  public function getRevisionAuthor(NodeInterface $revision) {
    return $revision->getRevisionUser()->getAccountName();
  }
}


<?php

namespace Drupal\sector_workflow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\content_moderation\Form\EntityModerationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ContentModerationBlock' block.
 *
 * @Block(
 *  id = "content_moderation_block",
 *  admin_label = @Translation("Content moderation block"),
 * )
 */
class ContentModerationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new ContentModerationBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandler $module_handler, FormBuilderInterface $form_builder, RouteMatchInterface $routeMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->formBuilder = $form_builder;
    $this->routeMatch = $routeMatch;

  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('form_builder'),
      $container->get('current_route_match'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->routeMatch->getParameter('node');
    if ($node && $node->hasField('moderation_state') && !$node->get('moderation_state')->isEmpty()) {
      // Further conditions to judge if we should render the form.
      // This all seems hacky. There must be a better option here than a massive if()..
      // TODO find out/think about it more.
      $validFormRender = $node->isLatestRevision() || $node->getRevisionId() == $node->id() || $node->isDefaultRevision();
      if ($validFormRender) {
        // Get the content moderation form render array.
        /** @phpstan-ignore-next-line */
        $contentModerationForm = $this->formBuilder->getForm(EntityModerationForm::class, $node);
        if (!empty($contentModerationForm)) {
          // Render the form into the block.
          $build['content_moderation_block'] = $contentModerationForm;
        }
      }
    }
    return $build;
  }

  public function getCacheMaxAge() {
    // TODO - We could probably cache this by Entity..
    return 0;
  }

}

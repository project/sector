<?php

namespace Drupal\sector_scroll_top\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "sector_scroll_top",
 *   admin_label = @Translation("Sector Custom Blocks › Scroll to top"),
 *   category = @Translation("Sector Custom Blocks")
 * )
 */
class ScrollToTop extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * Constructs a new ActiveMenusBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {


    $menu_output['#theme'] = 'sector_scroll_top_theme';

    return $menu_output;
  }


  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @see https://www.drupal.org/developing/api/8/cache/contexts.
    // If you depends on \Drupal::routeMatch().
    // You must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['sector_scroll_top']);
  }
}

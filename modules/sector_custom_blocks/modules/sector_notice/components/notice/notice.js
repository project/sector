(function(settings) {
    if(!settings.sector_notice) {
        return;
    }

    const { sector_notice: notices } = settings;

    Object.keys(notices).forEach(notice => {
        const dismissed = Cookies.get(`sector_notice.${notice}`);
        const elem = document.getElementById(notice);

        if (dismissed === undefined) {
            elem.removeAttribute('aria-hidden');
        }

        const { expiry } = notices[notice];

        const dismiss_btn = document.createElement('button');
        dismiss_btn.setAttribute('type', 'button')
        dismiss_btn.classList.add('notice__dismiss')
        dismiss_btn.innerHTML = `<span class="sr-only">Dismiss notice</span><span aria-hidden="true" class="material-icons-sharp material-symbols-sharp">close</span>`
        dismiss_btn.addEventListener('click', () => {
            Cookies.set(`sector_notice.${notice}`, true, { expires: parseInt(expiry) / 24 });
            elem.setAttribute('aria-hidden', true);
        });
        elem.querySelector('.block__content').appendChild(dismiss_btn)
    })
})(drupalSettings);
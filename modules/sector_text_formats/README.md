# Sector Text Formats

Note: The content moderator doesn't need the text format permissions. This role is additive and gets those permissions from other roles. (if that role has text format permissions the system falls apart because the editor all of a sudden can access full HTML if they are also Moderator, but that privilege is tight to the administrator role )

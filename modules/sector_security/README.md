# Sector Security module

The Sector Security module — part of the Starter Kit in Sector 10 — are built on the Drupal contribution modules:
Password Policy and Auto Logout

## Features and functionality
Sector Security includes:

- Password Policy module - it allows sitebuilders to configure granular password policies.
- Autologout module - logs users out after a specified time of inactivity. The time threshold can be set per user role, and a customisable logout dialog notifies the user of an impending logout.
- Content Security Policy module, included but not enabled by default, as this needs to be tailored to each site.

For more information, read https://www.sector.nz/documentation/sector-security

# Sector News

Bundles ..
* content type
* news Page node
* a news sample node
* news view /admin/structure/views/view/sector_news
* some out of the box meta /admin/config/search/metatag/node__sector_news
* CT content editor permissions

Upon uninstall -
* news Page node
* all `sector_news` nodes (including default content)
* news view
* the content type

will be deleted.